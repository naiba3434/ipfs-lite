package threads.lite;


import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.Network;
import threads.lite.core.AutonatResult;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.core.TimeoutCancellable;

@RunWith(AndroidJUnit4.class)
public class IpfsAutonatTest {
    private static final String TAG = IpfsAutonatTest.class.getSimpleName();


    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void autonat_test() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        Server server = TestEnv.getServer();
        assertNotNull(server);

        assertEquals(server.numServerConnections(), 0);

        Set<Multiaddr> addresses = server.publicNetworkAddresses();
        if (addresses.isEmpty()) {
            LogUtils.error(TAG, "Nothing to do, no public network addresses");
            return;
        }

        LogUtils.error(TAG, "Public addresses " + addresses);

        // find closest peers for testing autonat
        Set<Multiaddr> found = ConcurrentHashMap.newKeySet();

        try (Session session = ipfs.createSession()) {

            IPFS.findClosestPeers(session, ipfs.self(), found::add,
                    new TimeoutCancellable(() -> found.size() > 10, 30));

            for (Multiaddr multiaddr : found) {
                LogUtils.error(TAG, multiaddr.toString());
            }

            assertFalse(found.isEmpty());
        }


        AutonatResult result = server.autonat(found, 60);
        assertNotNull(result);
        LogUtils.error(TAG, result.toString());
    }
}

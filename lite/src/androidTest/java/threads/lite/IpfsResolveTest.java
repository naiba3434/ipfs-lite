package threads.lite;


import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Network;
import threads.lite.cid.PeerId;
import threads.lite.core.Page;
import threads.lite.core.PeerInfo;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.core.TimeoutCancellable;
import threads.lite.ipns.IpnsService;
import threads.lite.quic.Connection;

@RunWith(AndroidJUnit4.class)
public class IpfsResolveTest {
    private static final String TAG = IpfsResolveTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_resolve_publish() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        try (Session session = ipfs.createSession()) {
            String test = new String(TestEnv.getRandomBytes(20));
            Cid cid = IPFS.storeText(session, test);
            assertNotNull(cid);

            long sequence = System.currentTimeMillis();

            long start = System.currentTimeMillis();

            Set<Multiaddr> providers = ConcurrentHashMap.newKeySet();

            ipfs.publishName(session, cid, "homepage", providers::add,
                    new TimeoutCancellable(60));

            LogUtils.error(TAG, "Time provide " + (System.currentTimeMillis() - start) +
                    " number of providers " + providers.size());

            String key = ipfs.self().toBase36();

            Page entry = IPFS.resolvePage(session, PeerId.decode(key),
                    sequence, new TimeoutCancellable(60));
            assertNotNull(entry);
            assertEquals(ipfs.self(), entry.peerId());

            IPFS.findPeer(session, entry.peerId(),
                    multiaddr -> LogUtils.error(TAG, multiaddr.toString()),
                    new TimeoutCancellable(60));

            LogUtils.verbose(TAG, entry.toString());
            Cid cmp = IPFS.decodeIpnsData(entry);
            assertEquals(cmp, cid);
        }
    }


    @Test
    public void test_resolve_publish_manually() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        Server server = TestEnv.getServer();
        assertNotNull(server);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        try (Session session = ipfs.createSession()) {
            String test = new String(TestEnv.getRandomBytes(20));
            Cid cid = IPFS.storeText(session, test);
            assertNotNull(cid);

            long sequence = System.currentTimeMillis();

            long start = System.currentTimeMillis();

            Set<Multiaddr> multiaddrs = ConcurrentHashMap.newKeySet();
            IPFS.findClosestPeers(session, ipfs.self(), multiaddrs::add, new TimeoutCancellable(60));

            String name = "homepage";

            AtomicInteger providers = new AtomicInteger(0);
            for (Multiaddr multiaddr : multiaddrs) {
                try {
                    LogUtils.error(TAG, "try " + multiaddr.toString());

                    Connection connection = session.connect(multiaddr,
                            IPFS.getConnectionParameters());

                    LogUtils.error(TAG, "Success " + multiaddr);


                    PeerInfo info = ipfs.getPeerInfo(connection);


                    LogUtils.error(TAG, "Success 1 " + info);

                    IPFS.provide(server, connection, cid);

                    LogUtils.error(TAG, "Success 2 Publish Content ");

                    Set<PeerId> set = IPFS.getProvider(connection, cid);

                    LogUtils.error(TAG, "Success 2.5 Provider Set contains self " +
                            set.contains(ipfs.self()));

                    boolean result = ipfs.publishName(connection, cid, name);

                    LogUtils.error(TAG, "Success 3 Publish Name " + result);
                    if (result) {
                        providers.incrementAndGet();
                    }

                    if (providers.get() > 10) {
                        break;
                    }

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable.getClass().getSimpleName() +
                            " " + throwable.getMessage());
                }
            }


            LogUtils.error(TAG, "Time provide " + (System.currentTimeMillis() - start) +
                    " number of providers " + providers.get());

            assertTrue(providers.get() > 0);

            String key = ipfs.self().toBase36();

            Page entry = IPFS.resolvePage(session, PeerId.decode(key),
                    sequence, new TimeoutCancellable(60));
            assertNotNull(entry);
            assertEquals(ipfs.self(), entry.peerId());

            assertEquals(entry.name(), name);

            IPFS.findPeer(session, entry.peerId(),
                    multiaddr -> LogUtils.error(TAG, multiaddr.toString()),
                    new TimeoutCancellable(60));

            LogUtils.verbose(TAG, entry.toString());
            Cid cmp = IPFS.decodeIpnsData(entry);
            assertEquals(cmp, cid);
        }
    }

    @Test
    public void test_time_format() throws ParseException {
        @SuppressLint("SimpleDateFormat") String format = new SimpleDateFormat(
                IPFS.TIME_FORMAT_IPFS).format(new Date(System.currentTimeMillis()));
        assertNotNull(format);

        Date date = IpnsService.getDate(format);
        Objects.requireNonNull(date);


        Date cmp = IpnsService.getDate("2021-04-15T06:14:21.184394868Z");
        Objects.requireNonNull(cmp);

    }

    @Test
    public void test_peer_id() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        PeerId peerId = ipfs.self();
        assertNotNull(peerId);

        PeerId cmp = PeerId.decode(peerId.toString());

        assertEquals(cmp, peerId);

        byte[] key = IPFS.createIpnsKey(peerId);

        PeerId cmp1 = IPFS.decodeIpnsKey(key);

        assertEquals(cmp1, peerId);
    }

}

package threads.lite;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;

import threads.lite.cid.Cid;
import threads.lite.core.Page;
import threads.lite.core.PageStore;
import threads.lite.core.Session;


@RunWith(AndroidJUnit4.class)
public class IpfsPageTest {

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_page() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession(cid -> false)) {
            String content = "Hallo dfsadf";
            Cid text = IPFS.storeText(session, content);
            assertNotNull(text);
            PageStore pageStore = ipfs.getPageStore();

            Page page = pageStore.getPage(ipfs.self());
            assertNull(page);


            int sequence = 100;
            Date eol = Page.getDefaultEol();

            page = new Page(ipfs.self(), IPFS.LITE_PULL_PROTOCOL, "homepage",
                    eol.getTime(), Page.encodeIpnsData(text), sequence);

            assertNotNull(page);

            pageStore.storePage(page);

            page = pageStore.getPage(ipfs.self());
            assertNotNull(page);

            Cid cmp = Page.decodeIpnsData(page.value());
            assertEquals(text, cmp);

            Date eolCmp = page.getEolDate();
            assertEquals(eolCmp, eol);

            assertTrue(eolCmp.after(new Date(System.currentTimeMillis()))); // now time

            cmp = pageStore.getPageContent(ipfs.self());
            assertEquals(text, cmp);


            Thread.sleep(1000);
            pageStore.updatePageContent(ipfs.self(), text, Page.getDefaultEol());

            page = pageStore.getPage(ipfs.self());
            assertNotNull(page);

            eolCmp = page.getEolDate();
            assertNotEquals(eolCmp, eol); // time has changed

            assertEquals(page.protocol(), IPFS.LITE_PULL_PROTOCOL);
            assertEquals(page.name(), "homepage");

        }
    }

}
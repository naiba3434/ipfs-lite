package threads.lite.quic;

import androidx.annotation.NonNull;

import java.util.function.Consumer;

import lite.pb.Lite;
import threads.lite.LogUtils;


public record AlpnLiteResponder(
        @NonNull StreamResponder streamResponder) implements Consumer<StreamData> {
    private static final String TAG = AlpnLiteResponder.class.getSimpleName();


    public static AlpnLiteResponder create(@NonNull StreamResponder streamResponder) {
        return new AlpnLiteResponder(streamResponder);
    }

    @Override
    public void accept(StreamData rawData) {

        Stream stream = rawData.stream();

        if (rawData.isTerminated()) {
            LogUtils.error(TAG, "Stream " + stream.connection().remotePeerId() +
                    " is terminated");
            return;
        }

        try {
            Lite.ProtoSelect protoSelect = Lite.ProtoSelect.parseFrom(rawData.data());
            byte[] data = protoSelect.getData().toByteArray();
            streamResponder.data(stream, protoSelect.getProtocol(), data);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, "stream reading exception " + throwable.getMessage());
            stream.terminate();
            streamResponder.throwable(throwable);
        }
    }

}

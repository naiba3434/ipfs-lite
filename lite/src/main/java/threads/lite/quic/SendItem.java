package threads.lite.quic;


import java.util.function.Consumer;

record SendItem(Packet packet, Consumer<Packet> packetLostCallback) {
}


package threads.lite.crypto;

import androidx.annotation.NonNull;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;

import crypto.pb.Crypto;


public interface Rsa {

    static PubKey unmarshalRsaPublicKey(byte[] keyBytes) throws Exception {

        PublicKey publicKey = KeyFactory.getInstance("RSA")
                .generatePublic(new X509EncodedKeySpec(keyBytes));
        return new RsaPublicKey(publicKey);

    }


    record RsaPublicKey(PublicKey publicKey) implements PubKey {

        @NonNull
        public byte[] raw() {
            return this.publicKey.getEncoded();
        }

        public void verify(byte[] data, byte[] signature) throws Exception {

            Signature sha256withRSA = Signature.getInstance("SHA256withRSA");
            sha256withRSA.initVerify(this.publicKey);
            sha256withRSA.update(data);
            boolean result = sha256withRSA.verify(signature);
            if (!result) {
                throw new Exception("verify failed");
            }
        }

        @NonNull
        @Override
        public Crypto.KeyType getKeyType() {
            return Crypto.KeyType.RSA;
        }


    }
}

package threads.lite.ident;

import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.core.DataHandler;
import threads.lite.core.Host;
import threads.lite.core.ProtocolHandler;
import threads.lite.quic.Stream;

public final class IdentityPushHandler implements ProtocolHandler {

    private final Host host;

    public IdentityPushHandler(Host host) {
        this.host = host;
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        stream.writeOutput(DataHandler.encodeProtocols(IPFS.IDENTITY_PUSH_PROTOCOL), true);
    }

    @Override
    public void data(String alpn, Stream stream, byte[] data) throws Exception {
        // just read out the identity (later store them into the connection
        // when a use-case is there

        // TODO [Future low] when invoked store them on the connection or somewhere else
        IdentifyOuterClass.Identify identify = IdentifyOuterClass.Identify.parseFrom(data);
        IdentityService.getPeerInfo(host.self(), identify, stream.connection().remotePeerId());

    }

    @Override
    public void fin(Stream stream) {
        // this is invoked, that is good, but request is finished when message is parsed [ok]
        LogUtils.error(getClass().getSimpleName(), stream.connection().remotePeerId().toString()); // todo
    }

}

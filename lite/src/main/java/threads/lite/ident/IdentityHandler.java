package threads.lite.ident;

import threads.lite.IPFS;
import threads.lite.core.DataHandler;
import threads.lite.core.Host;
import threads.lite.core.ProtocolHandler;
import threads.lite.quic.Stream;

public final class IdentityHandler implements ProtocolHandler {

    private final Host host;

    public IdentityHandler(Host host) {
        this.host = host;
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        stream.writeOutput(DataHandler.encode(host.identify(), IPFS.IDENTITY_PROTOCOL), true);
    }

    @Override
    public void data(String alpn, Stream stream, byte[] data) throws Exception {
        throw new Exception("should not be invoked");
    }

    @Override
    public void fin(Stream stream) {
        // this is invoked, that is good, but request is finished when message is parsed [ok]
    }

}

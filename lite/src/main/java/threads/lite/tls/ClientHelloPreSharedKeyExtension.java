/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package threads.lite.tls;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * TLS Pre-Shared Key Extension, ClientHello variant.
 * see <a href="https://datatracker.ietf.org/doc/html/rfc8446#section-4.2.11">...</a>
 */
public record ClientHelloPreSharedKeyExtension(PskIdentity[] identities,
                                               PskBinderEntry[] binders,
                                               int binderPosition) implements PreSharedKeyExtension {

    private static final int MINIMUM_EXTENSION_DATA_SIZE = 2 + 2 + 1 + 4 + 2 + 1 + 32;


    public static ClientHelloPreSharedKeyExtension parse(ByteBuffer buffer) throws DecodeErrorException {
        int startPosition = buffer.position();
        int extensionDataLength = Extension.parseExtensionHeader(
                buffer, TlsConstants.ExtensionType.pre_shared_key, MINIMUM_EXTENSION_DATA_SIZE);

        List<PskIdentity> identities = new ArrayList<>();
        int remainingIdentitiesLength = buffer.getShort() & 0xffff;
        int remaining = extensionDataLength - 2;
        while (remainingIdentitiesLength > 0) {
            if (remaining < 2) {
                throw new DecodeErrorException("Incomplete psk identity");
            }
            int identityLength = buffer.getShort() & 0xffff;
            remaining -= 2;
            if (identityLength > remaining) {
                throw new DecodeErrorException("Incorrect identity length value");
            }
            byte[] identity = new byte[identityLength];
            buffer.get(identity);
            remaining -= identityLength;
            if (remaining < 4) {
                throw new DecodeErrorException("Incomplete psk identity");
            }
            int obfuscatedTicketAge = buffer.getInt();
            remaining -= 4;
            identities.add(new PskIdentity(identity, obfuscatedTicketAge));
            remainingIdentitiesLength -= (2 + identityLength + 4);
        }
        if (remainingIdentitiesLength != 0) {
            throw new DecodeErrorException("Incorrect identities length value");
        }

        int binderPosition = buffer.position() - startPosition;
        List<PskBinderEntry> binders = new ArrayList<>();
        if (remaining < 2) {
            throw new DecodeErrorException("Incomplete binders");
        }
        int bindersLength = buffer.getShort() & 0xffff;
        remaining -= 2;
        while (bindersLength > 0) {
            if (remaining < 1) {
                throw new DecodeErrorException("Incorrect binder value");
            }
            int binderLength = buffer.get() & 0xff;
            remaining -= 1;
            if (binderLength > remaining) {
                throw new DecodeErrorException("Incorrect binder length value");
            }
            if (binderLength < 32) {
                throw new DecodeErrorException("Invalid binder length");
            }
            byte[] hmac = new byte[binderLength];
            buffer.get(hmac);
            remaining -= binderLength;
            binders.add(new PskBinderEntry(hmac));
            bindersLength -= (1 + binderLength);
        }
        if (bindersLength != 0) {
            throw new DecodeErrorException("Incorrect binders length value");
        }
        if (remaining > 0) {
            throw new DecodeErrorException("Incorrect extension data length value");
        }
        if (identities.size() != binders.size()) {
            throw new DecodeErrorException("Inconsistent number of identities vs binders");
        }
        if (identities.size() == 0) {
            throw new DecodeErrorException("Empty OfferedPsks");
        }

        PskIdentity[] pskIdentities = new PskIdentity[identities.size()];
        PskBinderEntry[] binderEntries = new PskBinderEntry[binders.size()];
        return new ClientHelloPreSharedKeyExtension(
                identities.toArray(pskIdentities),
                binders.toArray(binderEntries), binderPosition);
    }

    @Override
    public byte[] getBytes() {
        throw new IllegalArgumentException();
    }


    private record PskIdentity(byte[] identity, long obfuscatedTicketAge) {
    }

    private record PskBinderEntry(byte[] hmac) {
    }
}

/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package threads.lite.tls;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import threads.lite.LogUtils;

public interface HandshakeMessage extends Message {
    String TAG = HandshakeMessage.class.getSimpleName();

    static Extension[] parseExtensions(ByteBuffer buffer, HandshakeType context) throws ErrorAlert {
        return parseExtensions(buffer, context, null);
    }

    static Extension[] parseExtensions(ByteBuffer buffer, HandshakeType context, ExtensionParser customExtensionParser) throws ErrorAlert {
        if (buffer.remaining() < 2) {
            throw new DecodeErrorException("Extension field must be at least 2 bytes long");
        }
        ArrayList<Extension> extensions = new ArrayList<>();

        int remainingExtensionsLength = buffer.getShort() & 0xffff;
        if (buffer.remaining() < remainingExtensionsLength) {
            throw new DecodeErrorException("Extensions too short");
        }

        while (remainingExtensionsLength >= 4) {
            buffer.mark();
            int extensionType = buffer.getShort() & 0xffff;
            int extensionLength = buffer.getShort() & 0xffff;
            remainingExtensionsLength -= 4;
            buffer.reset();
            if (extensionLength > remainingExtensionsLength) {
                throw new DecodeErrorException("Extension length exceeds extensions length");
            }
            int extensionStartPosition = buffer.position();

            if (extensionType == TlsConstants.ExtensionType.server_name.value) {
                extensions.add(ServerNameExtension.parse(buffer));
            } else if (extensionType == TlsConstants.ExtensionType.supported_groups.value) {
                extensions.add(SupportedGroupsExtension.parse(buffer));
            } else if (extensionType == TlsConstants.ExtensionType.signature_algorithms.value) {
                extensions.add(SignatureAlgorithmsExtension.parse(buffer));
            } else if (extensionType == TlsConstants.ExtensionType.application_layer_protocol_negotiation.value) {
                extensions.add(ApplicationLayerProtocolNegotiationExtension.parse(buffer));
            } else if (extensionType == TlsConstants.ExtensionType.pre_shared_key.value) {
                if (context == HandshakeType.server_hello) {
                    extensions.add(ServerPreSharedKeyExtension.parse(buffer));
                } else if (context == HandshakeType.client_hello) {
                    extensions.add(ClientHelloPreSharedKeyExtension.parse(buffer));
                } else {
                    throw new IllegalParameterAlert("Extension not allowed in " +
                            HandshakeType.get(context.value));
                }
            } else if (extensionType == TlsConstants.ExtensionType.early_data.value) {
                extensions.add(EarlyDataExtension.parse(buffer, context));
            } else if (extensionType == TlsConstants.ExtensionType.supported_versions.value) {
                extensions.add(SupportedVersionsExtension.parse(buffer, context));
            } else if (extensionType == TlsConstants.ExtensionType.psk_key_exchange_modes.value) {
                extensions.add(PskKeyExchangeModesExtension.parse(buffer));
            } else if (extensionType == TlsConstants.ExtensionType.certificate_authorities.value) {
                extensions.add(CertificateAuthoritiesExtension.parse(buffer));
            } else if (extensionType == TlsConstants.ExtensionType.key_share.value) {
                extensions.add(KeyShareExtension.create(buffer, context));
            } else {
                Extension extension = null;
                if (customExtensionParser != null) {
                    extension = customExtensionParser.apply(buffer, context);
                }
                if (extension != null) {
                    extensions.add(extension);
                } else {
                    LogUtils.warning(TAG, "Unsupported extension, type is: " + extensionType);
                    extensions.add(UnknownExtension.parse(buffer));
                }
            }
            if (buffer.position() - extensionStartPosition != 4 + extensionLength) {
                throw new DecodeErrorException("Incorrect extension length");
            }
            remainingExtensionsLength -= extensionLength;
        }

        Extension[] extensionsArray = new Extension[extensions.size()];

        return extensions.toArray(extensionsArray);
    }

    /**
     * Returns the (relative) position of the last extension.
     *
     * @param buffer data to parse, buffer position should be at the point where extensions start.
     *               on return, the buffer position will be right after the last extension.
     * @return the start position of the last extension, relative to the start of all extensions.
     */
    static int findPositionLastExtension(ByteBuffer buffer) {
        int remaining = buffer.getShort() & 0xffff;
        int lastExtensionStart = 0;
        while (remaining > 4) {
            lastExtensionStart = buffer.position();
            buffer.getShort(); // read type
            int length = buffer.getShort() & 0xffff;
            buffer.get(new byte[length]);
            remaining -= (2 + 2 + length);
        }
        return lastExtensionStart;
    }

    static int parseHandshakeHeader(ByteBuffer buffer, HandshakeType expectedType, int minimumMessageSize) throws DecodeErrorException {
        if (buffer.remaining() < 4) {
            throw new DecodeErrorException("handshake message underflow");
        }
        int handshakeType = buffer.get() & 0xff;
        if (handshakeType != expectedType.value) {
            throw new IllegalStateException();  // i.e. programming error
        }
        int messageDataLength = ((buffer.get() & 0xff) << 16) | ((buffer.get() & 0xff) << 8) | (buffer.get() & 0xff);
        if (4 + messageDataLength < minimumMessageSize) {
            throw new DecodeErrorException(HandshakeMessage.class.getSimpleName() + " can't be less than " + minimumMessageSize + " bytes");
        }
        if (buffer.remaining() < messageDataLength) {
            throw new DecodeErrorException("handshake message underflow");
        }
        return messageDataLength;
    }

    HandshakeType getType();

    byte[] getBytes();

}

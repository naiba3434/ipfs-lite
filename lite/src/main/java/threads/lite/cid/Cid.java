package threads.lite.cid;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.TypeConverter;

import com.google.protobuf.CodedInputStream;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Objects;

import threads.lite.core.DataHandler;

public record Cid(Multihash multihash, int codec) {

    @Nullable
    @TypeConverter
    public static Cid fromArray(byte[] data) {
        if (data == null) {
            return null;
        }
        return decode(data);
    }

    @Nullable
    @TypeConverter
    public static byte[] toArray(Cid cid) {
        if (cid == null) {
            return null;
        }
        return cid.encoded();
    }

    @NonNull
    public static Cid nsToCid(@NonNull String ns) throws Exception {
        return createCidV1(Multicodec.RAW, ns.getBytes(StandardCharsets.UTF_8));
    }

    @NonNull
    public static Cid decode(@NonNull String name) throws Exception {
        if (name.length() < 2) {
            throw new IllegalStateException("invalid cid");
        }

        if (name.length() == 46 && name.startsWith("Qm")) {
            Multihash multihash = Multihash.decode(Base58.decode(name));
            Objects.requireNonNull(multihash);
            return new Cid(multihash, Multicodec.DagProtobuf.codec());
        }

        byte[] raw = Multibase.decode(name);

        CodedInputStream codedInputStream = CodedInputStream.newInstance(raw);

        int version = codedInputStream.readRawVarint32();
        if (version != 1) {
            throw new IllegalStateException("invalid version");
        }
        int codecType = codedInputStream.readRawVarint32();
        Multicodec ipld = Multicodec.get(codecType);

        Multihash mh = Multihash.decode(codedInputStream);
        Objects.requireNonNull(mh);
        return new Cid(mh, ipld.codec());
    }

    @NonNull
    public static Cid createCidV1(Multicodec ipld, byte[] data) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(data);
        Multihash multihash = Multihash.create(Hashtype.SHA2_256, hash);
        return createCidV1(ipld, multihash);
    }

    @NonNull
    public static Cid createCidV1(Multicodec ipld, Multihash multihash) {
        return new Cid(multihash, ipld.codec());
    }

    @NonNull
    public static Cid decode(byte[] data) {
        try {
            if (data[0] == 1) { // can only be version cid version 1
                CodedInputStream codedInputStream = CodedInputStream.newInstance(data);
                codedInputStream.readRawVarint32(); // skip version, because it is 1
                int codecType = codedInputStream.readRawVarint32();
                Multicodec multicodec = Multicodec.get(codecType);
                Multihash mh = Multihash.decode(codedInputStream);
                Objects.requireNonNull(mh);
                return new Cid(mh, multicodec.codec());
            } else {
                return Cid.createCidV1(Multicodec.DagProtobuf, Multihash.decode(data));
            }
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cid cid = (Cid) o;
        return codec == cid.codec && multihash.equals(cid.multihash);
    }

    @Override
    public int hashCode() {
        return Objects.hash(multihash, codec); // ok, checked, maybe opt
    }

    @NonNull
    @Override
    public String toString() {
        return Multibase.encode(Multibase.BASE32, encoded());
    }

    @NonNull
    public Multicodec getCodec() {
        return Multicodec.get(codec);
    }

    public byte[] encoded() {
        byte[] encoded = multihash.encoded();
        int versionLength = DataHandler.unsignedVariantSize(1);
        int codecLength = DataHandler.unsignedVariantSize(codec);

        ByteBuffer buffer = ByteBuffer.allocate(
                versionLength + codecLength + encoded.length);
        DataHandler.writeUnsignedVariant(buffer, 1);
        DataHandler.writeUnsignedVariant(buffer, codec);
        buffer.put(encoded);
        return buffer.array();

    }

    @NonNull
    public Prefix getPrefix() {
        Hashtype type = multihash.type();
        return new Prefix(getCodec(), type, 1);

    }

    public boolean isSupported() {
        return multihash.type() == Hashtype.SHA2_256;
    }
}
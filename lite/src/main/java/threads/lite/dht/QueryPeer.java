package threads.lite.dht;

import androidx.annotation.NonNull;

import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicReference;

import threads.lite.cid.ID;
import threads.lite.cid.Peer;


public record QueryPeer(@NonNull Peer peer, @NonNull BigInteger distance,
                        @NonNull AtomicReference<PeerState> state)
        implements Comparable<QueryPeer> {


    public static QueryPeer create(@NonNull Peer peer, @NonNull ID key) {
        BigInteger distance = QueryPeer.distance(key, peer.id());
        return new QueryPeer(peer, distance, new AtomicReference<>(PeerState.PeerHeard));
    }

    private static byte[] xor(byte[] x1, byte[] x2) {
        byte[] out = new byte[x1.length];

        for (int i = 0; i < x1.length; i++) {
            out[i] = (byte) (0xff & ((int) x1[i]) ^ ((int) x2[i]));
        }
        return out;
    }

    @NonNull
    public static BigInteger distance(@NonNull ID a, @NonNull ID b) {
        byte[] k3 = xor(a.data(), b.data());

        // SetBytes interprets buf as the bytes of a big-endian unsigned
        // integer, sets z to that value, and returns z.
        // big.NewInt(0).SetBytes(k3)

        return new BigInteger(k3);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QueryPeer queryPeer = (QueryPeer) o;
        return peer.equals(queryPeer.peer) && distance.equals(queryPeer.distance);
    }

    @NonNull
    public PeerState getState() {
        return state.get();
    }

    public void setState(@NonNull PeerState state) {
        this.state.set(state);
    }

    @NonNull
    @Override
    public String toString() {
        return "QueryPeer{" +
                "id=" + peer.getPeerId() +
                ", distance=" + distance +
                ", state=" + state +
                '}';
    }

    @Override
    public int compareTo(QueryPeer o) {
        return distance.compareTo(o.distance);
    }


}

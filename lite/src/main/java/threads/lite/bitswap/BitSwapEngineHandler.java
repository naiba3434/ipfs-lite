package threads.lite.bitswap;

import bitswap.pb.MessageOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.core.DataHandler;
import threads.lite.core.ProtocolHandler;
import threads.lite.quic.Stream;

public final class BitSwapEngineHandler implements ProtocolHandler {

    private final BitSwapEngine bitSwapEngine;

    public BitSwapEngineHandler(BitSwapEngine bitSwapEngine) {
        this.bitSwapEngine = bitSwapEngine;
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        stream.writeOutput(DataHandler.encodeProtocols(IPFS.BITSWAP_PROTOCOL), true);
    }

    @Override
    public void data(String alpn, Stream stream, byte[] data) throws Exception {
        bitSwapEngine.receiveMessage(stream.connection(),
                MessageOuterClass.Message.parseFrom(data));
    }

    @Override
    public void fin(Stream stream) {
        // this is invoked, that is good, but request is finished when message is parsed [ok]
        LogUtils.error(getClass().getSimpleName(), "fin received " +
                stream.connection().remotePeerId());
    }


}

package threads.lite.host;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.net.ConnectException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

import bitswap.pb.MessageOuterClass;
import crypto.pb.Crypto;
import identify.pb.IdentifyOuterClass;
import record.pb.RecordOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.bitswap.BitSwapHandler;
import threads.lite.bitswap.BitSwapManager;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.cid.IPV;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.BitSwap;
import threads.lite.core.BlockStore;
import threads.lite.core.Cancellable;
import threads.lite.core.MultistreamHandler;
import threads.lite.core.Page;
import threads.lite.core.PeerStore;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Session;
import threads.lite.core.Swap;
import threads.lite.dht.DhtKademlia;
import threads.lite.ident.IdentityHandler;
import threads.lite.ident.IdentityPushHandler;
import threads.lite.ident.IdentityService;
import threads.lite.quic.Connection;
import threads.lite.quic.ConnectionBuilder;
import threads.lite.quic.Parameters;
import threads.lite.relay.RelayService;
import threads.lite.swap.LiteSwapHandler;
import threads.lite.swap.LiteSwapManager;


public final class LiteSession implements Session {
    private static final String TAG = LiteSession.class.getSimpleName();
    @NonNull
    private final BitSwap bitSwap;
    @NonNull
    private final Swap swap;
    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final DhtKademlia routing;
    @NonNull
    private final AtomicBoolean closed = new AtomicBoolean(false);
    @NonNull
    private final Function<Cid, Boolean> isBitswapActive;
    @NonNull
    private final LiteHost host;
    @NonNull
    private final LiteResponder liteResponder;
    @NonNull
    private final ConcurrentHashMap<PeerId, Connection> swarm = new ConcurrentHashMap<>();
    @NonNull
    private final AtomicReference<IdentifyOuterClass.Identify> identify = new AtomicReference<>();
    @NonNull
    private final IdentifyUpdater identifyUpdater = new IdentifyUpdater();

    LiteSession(@NonNull BlockStore blockStore, @NonNull LiteHost host,
                @NonNull Function<Cid, Boolean> isBitswapActive) {
        this.blockStore = blockStore;
        this.host = host;
        this.isBitswapActive = isBitswapActive;
        this.bitSwap = new BitSwapManager(this);
        this.swap = new LiteSwapManager(this);
        this.routing = new DhtKademlia(this);


        Map<String, ProtocolHandler> protocols = Map.of(
                IPFS.MULTISTREAM_PROTOCOL, new MultistreamHandler(),
                IPFS.LITE_PUSH_PROTOCOL, new LitePushHandler(host),
                IPFS.LITE_PULL_PROTOCOL, new LitePullHandler(host),
                IPFS.LITE_SWAP_PROTOCOL, new LiteSwapHandler(host),
                IPFS.IDENTITY_PROTOCOL, new IdentityHandler(this),
                IPFS.IDENTITY_PUSH_PROTOCOL, new IdentityPushHandler(this),
                IPFS.BITSWAP_PROTOCOL, new BitSwapHandler(this));

        this.liteResponder = new LiteResponder(protocols);

    }

    @NonNull
    public LiteResponder getLiteResponder() {
        return liteResponder;
    }

    @Override
    public byte[] getPrivateKey() {
        return host.getKeys().privateKey();
    }

    @NonNull
    public Set<Connection> swarm() {
        Set<Connection> result = new HashSet<>();
        for (Connection connection : swarm.values()) {
            if (connection.isConnected()) {
                result.add(connection);
            } else {
                swarm.remove(connection.remotePeerId());
            }
        }
        return result;
    }

    @SuppressWarnings("unused")
    @Override
    public void closeConnection(@Nullable Connection connection) {
        try {
            if (connection != null) {
                connection.close();
                swarm.remove(connection.remotePeerId());
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @NonNull
    public Connection connect(Multiaddr multiaddr, Parameters parameters)
            throws ConnectException, InterruptedException, TimeoutException {
        Connection connection = getConnection(multiaddr.peerId());
        if (connection != null) {
            return connection;
        }
        connection = ConnectionBuilder.connect(liteResponder,
                multiaddr, parameters, host.getLiteCertificate(), ipv());
        addSwarmConnection(connection);
        return connection;
    }

    @Nullable
    @Override
    public Connection getConnection(PeerId peerId) {
        Connection connection = swarm.get(peerId);
        if (connection != null) {
            if (connection.isConnected()) {
                return connection;
            } else {
                swarm.remove(peerId);
            }
        }
        return null;
    }

    @NonNull
    public Map<String, ProtocolHandler> getProtocols() {
        return liteResponder.protocols();
    }

    @Override
    @NonNull
    public Block getBlock(@NonNull Cancellable cancellable, @NonNull Cid cid)
            throws Exception {
        if (isBitswapActive.apply(cid)) {
            return bitSwap.getBlock(cancellable, cid);
        } else {
            return swap.getBlock(cancellable, cid);
        }
    }

    @Override
    public void receiveMessage(@NonNull Connection connection,
                               @NonNull MessageOuterClass.Message bsm) throws Exception {
        bitSwap.receiveMessage(connection, bsm);
    }

    @NonNull
    @Override
    public BlockStore getBlockStore() {
        return blockStore;
    }

    @NonNull
    @Override
    public PeerStore getPeerStore() {
        return host.getPeerStore();
    }

    @Override
    public PeerId self() {
        return host.self();
    }

    @Override
    public void putValue(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull RecordOuterClass.Record record) {
        routing.putValue(cancellable, consumer, record);
    }

    @Override
    public void findPeer(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull PeerId peerId) {
        routing.findPeer(cancellable, consumer, peerId);
    }

    @Override

    public void searchValue(@NonNull Cancellable cancellable,
                            @NonNull Consumer<Page> consumer, @NonNull byte[] key) {
        routing.searchValue(cancellable, consumer, key);
    }

    @Override
    public void findProviders(@NonNull Cancellable cancellable,
                              @NonNull Consumer<Multiaddr> consumer,
                              @NonNull Cid cid) {
        routing.findProviders(cancellable, consumer, cid);
    }

    @Override
    @NonNull
    public Connection dial(Multiaddr address, Parameters parameters)
            throws ConnectException, InterruptedException, TimeoutException {

        Connection connection = getConnection(address.peerId());
        if (connection != null) {
            return connection;
        }

        if (address.isCircuitAddress()) {
            try {
                return RelayService.directConnection(this, this, address, parameters);
            } catch (InterruptedException | ConnectException | TimeoutException exception) {
                throw exception;
            } catch (Throwable throwable) {
                throw new ConnectException(throwable.getMessage());
            }
        } else {
            return connect(address, parameters);
        }
    }

    @NonNull
    @Override
    public LiteCertificate getLiteCertificate() {
        return host.getLiteCertificate();
    }

    @Override
    public void findClosestPeers(@NonNull Cancellable cancellable,
                                 @NonNull Consumer<Multiaddr> consumer, @NonNull PeerId peerId) {
        routing.findClosestPeers(cancellable, consumer, peerId);
    }

    @NonNull
    @Override
    public Set<Peer> getRoutingPeers() {
        return host.getRoutingPeers();
    }

    @Override
    public void close() {
        try {
            swarm.values().forEach(Connection::close);
            bitSwap.close();
            swap.close();
            routing.close();
            swarm.clear(); // connections are closed in bitswap anyway
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            closed.set(true);
        }
    }

    @Override
    public boolean isClosed() {
        return closed.get();
    }

    @Override
    public Multiaddrs publishMultiaddrs() {
        return new Multiaddrs(); // nothing to publish
    }

    @Override
    public Crypto.PublicKey getPublicKey() {
        return host.getPublicKey();
    }

    @NonNull
    private Set<String> getProtocolNames() {
        return liteResponder.getProtocolNames();
    }

    @Override
    public void addSwarmConnection(Connection connection) {
        swarm.put(connection.remotePeerId(), connection);
    }

    @NonNull
    @Override
    public Function<Cid, Boolean> isBitswapActive() {
        return isBitswapActive;
    }

    @Override
    public IdentifyOuterClass.Identify identify() {
        return identify.updateAndGet(identifyUpdater);
    }

    @Override
    public Supplier<IPV> ipv() {
        return host.ipv();
    }

    private class IdentifyUpdater implements UnaryOperator<IdentifyOuterClass.Identify> {
        @Override
        public IdentifyOuterClass.Identify apply(IdentifyOuterClass.Identify identify) {
            if (identify == null) {
                return IdentityService.createOwnIdentity(
                        self(), getPrivateKey(), getPublicKey(),
                        getProtocolNames(), publishMultiaddrs());
            }
            return identify;
        }
    }
}


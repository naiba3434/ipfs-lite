package threads.lite.relay;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.net.ConnectException;
import java.net.DatagramSocket;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import circuit.pb.Circuit;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.DataHandler;
import threads.lite.core.Host;
import threads.lite.core.Limit;
import threads.lite.core.PeerInfo;
import threads.lite.core.Reservation;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.holepunch.HolePunch;
import threads.lite.holepunch.HolePunchInfo;
import threads.lite.host.LiteHost;
import threads.lite.ident.IdentityService;
import threads.lite.quic.Connection;
import threads.lite.quic.ConnectionBuilder;
import threads.lite.quic.Parameters;
import threads.lite.quic.Stream;
import threads.lite.quic.StreamRequester;

public interface RelayService {

    String TAG = RelayService.class.getSimpleName();

    @NonNull
    static Connection directConnection(@NonNull Host host, @NonNull Session session,
                                       @NonNull Multiaddr address, @NonNull Parameters parameters)
            throws Exception {

        PeerId peerId = address.peerId();

        Multiaddr relayAddress = address.getRelayAddress();
        PeerId relayId = relayAddress.peerId();
        Objects.requireNonNull(relayId);

        DatagramSocket socket = new DatagramSocket(LiteHost.nextFreePort());

        // relay is not added to the swarm (only temporary connection, till hole punch is done
        // or failed
        Connection connection = ConnectionBuilder.connect(host.getLiteResponder(),
                relayAddress, Parameters.getDefault(), host.getLiteCertificate(),
                host.ipv());

        try {
            PeerInfo peerInfo = IdentityService.getPeerInfo(host.self(), connection);

            if (!peerInfo.hasRelayHop()) {
                throw new ConnectException("No relay hop protocol [abort]");
            }

            Multiaddr observed = peerInfo.observed();
            Objects.requireNonNull(observed);
            if (observed.isCircuitAddress()) {
                throw new ConnectException("Observed address is circuit address [abort]");
            }

            // check if observed address has same port then connection [validity check]
            // when ports are not equal, no hole punching is possible [kind of pre-condition]
            // fast symmetric NAT test
            boolean enableHolePunch = Objects.equals(observed.port(),
                    connection.localAddress().getPort());

            if (!enableHolePunch) {
                throw new ConnectException("Hole punching not possible [abort]");
            }

            // calculate final observed address for hole punching
            Multiaddr derivedObserved = Multiaddr.create(peerId, observed.inetAddress(),
                    socket.getLocalPort());
            LogUtils.error(TAG, "Derived observed address " + derivedObserved);


            // check if there is a reservation to the peerId
            // then return a relayed connection, otherwise abort
            // and remove relayed connection from swarm

            HolePunchInfo holePunchInfo = new HolePunchInfo(host.ipv(), derivedObserved,
                    socket, parameters);

            return directConnectionToPeer(session, connection, holePunchInfo)
                    .get(IPFS.RELAY_CONNECT_TIMEOUT, TimeUnit.SECONDS);
            // now we have a direct connection to the given peerId
        } catch (Throwable throwable) {
            socket.close();
            throw throwable;
        } finally {
            connection.close(); // in any case connection will be closed to relay
        }
    }

    @NonNull
    static Reservation reservation(@NonNull Server server,
                                   @NonNull Multiaddr multiaddr) throws Exception {

        Connection connection = server.connect(multiaddr, Parameters.getDefault(true));

        try {
            PeerInfo peerInfo = IdentityService.getPeerInfo(server.self(), connection);
            if (!peerInfo.hasRelayHop()) {
                connection.close();
                throw new Exception("No relay hop protocol [abort]");
            }

            return reservation(server.self(), connection, multiaddr);

        } catch (Throwable throwable) {
            connection.close();
            throw throwable;
        }
    }


    static Reservation reservation(
            @NonNull PeerId self, @NonNull Connection connection,
            @NonNull Multiaddr relayAddress) throws Exception {


        Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                .setType(Circuit.HopMessage.Type.RESERVE).build();

        CompletableFuture<Reservation> done = new CompletableFuture<>();


        StreamRequester.createStream(connection, new HopRequest(done, relayAddress, self))
                .request(DataHandler.encode(message, IPFS.MULTISTREAM_PROTOCOL,
                        IPFS.RELAY_PROTOCOL_HOP), IPFS.DEFAULT_REQUEST_TIMEOUT);

        return done.get(IPFS.DEFAULT_REQUEST_TIMEOUT, TimeUnit.SECONDS);
    }

    static CompletableFuture<Connection> directConnectionToPeer(
            @NonNull Session session, @NonNull Connection connection,
            @NonNull HolePunchInfo holePunchInfo) {


        CompletableFuture<Connection> done = new CompletableFuture<>();

        try {
            StreamRequester.createStream(connection, new RelayHopRequest(done, session, holePunchInfo))
                    .writeOutput(DataHandler.encodeProtocols(
                            IPFS.MULTISTREAM_PROTOCOL, IPFS.RELAY_PROTOCOL_HOP), false);

        } catch (Throwable throwable) {
            done.completeExceptionally(throwable);
        }
        return done;
    }

    static Limit getLimit(Circuit.HopMessage hopMessage) {
        long limitData = 0;
        long limitDuration = 0;
        boolean limited = false;

        if (hopMessage.hasLimit()) {
            Circuit.Limit limit = hopMessage.getLimit();
            limitData = limit.getData();
            limitDuration = limit.getDuration();
            limited = true;
        }

        return new Limit(limitData, limitDuration, limited);
    }

    record RelayHopRequest(CompletableFuture<Connection> done, Session session,
                           HolePunchInfo holePunchInfo) implements StreamRequester {

        @Override
        public void throwable(Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        @Override
        public void fin(Stream stream) {
            // this is invoked, that is good, but request is finished when message is parsed [ok]
            LogUtils.error(getClass().getSimpleName(), "fin received " +
                    stream.connection().remotePeerId());
        }


        @Override
        public void terminated() {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream terminated"));
            }
        }

        @Override
        public void protocol(Stream stream, String protocol) {

            stream.setAttribute(PROTOCOL, protocol);

            switch (protocol) {
                case IPFS.MULTISTREAM_PROTOCOL:
                    if (!stream.isInitiator()) {
                        stream.writeOutput(DataHandler.encodeProtocols(
                                IPFS.MULTISTREAM_PROTOCOL), false);
                    }
                    break;

                case IPFS.RELAY_PROTOCOL_HOP:
                    Circuit.Peer dest = Circuit.Peer.newBuilder()
                            .setId(ByteString.copyFrom(holePunchInfo.getPeerId().encoded()))
                            .build();

                    Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                            .setType(Circuit.HopMessage.Type.CONNECT)
                            .setPeer(dest)
                            .build();
                    stream.writeOutput(DataHandler.encode(message), false);
                    break;
                case IPFS.HOLE_PUNCH_PROTOCOL:
                    // nothing to do here
                    break;
                default:
                    LogUtils.error(TAG, "Ignore " + protocol);
                    stream.writeOutput(DataHandler.encodeProtocols(IPFS.NA), false);

            }
        }

        @Override
        public void data(Stream stream, byte[] data) throws Exception {

            String protocol = (String) stream.getAttribute(PROTOCOL);
            LogUtils.error(TAG, "data streamId " + stream.getStreamId() +
                    " protocol " + protocol + " initiator " + stream.isInitiator());
            Objects.requireNonNull(protocol);
            switch (protocol) {
                case IPFS.RELAY_PROTOCOL_HOP -> {
                    Circuit.HopMessage msg = Circuit.HopMessage.parseFrom(data);
                    Objects.requireNonNull(msg);
                    if (msg.getType() != Circuit.HopMessage.Type.STATUS) {
                        throw new Exception(msg.getType().name());
                    }

                    if (msg.getStatus() != Circuit.Status.OK) {
                        throw new Exception(msg.getStatus().name());
                    }

                    Limit limit = RelayService.getLimit(msg);
                    Objects.requireNonNull(limit);
                    // Note the limit will not be checked if it a non limited relay
                    // always a hole punch will be done

                    // do directly the punch hole, instead of noise, etc.
                    stream.writeOutput(DataHandler.encodeProtocols(
                            IPFS.MULTISTREAM_PROTOCOL, IPFS.HOLE_PUNCH_PROTOCOL), false);


                }
                case IPFS.HOLE_PUNCH_PROTOCOL -> {

                    Multiaddr multiaddr = HolePunch.response(holePunchInfo, stream, data);

                    if (multiaddr != null) {
                        // the creating of a connection should not block, it will otherwise block
                        // all reading of the underlying port
                        // TODO [Future urgent] replace by virtual threads
                        Executors.newSingleThreadExecutor().execute(() -> {
                            try {

                                Connection connection = ConnectionBuilder.connect(
                                        session.getLiteResponder(), multiaddr,
                                        holePunchInfo.parameters(),
                                        session.getLiteCertificate(),
                                        session.ipv(),
                                        holePunchInfo.socket());
                                session.addSwarmConnection(connection);

                                LogUtils.error(TAG, "[A] Success Hole Punching to [B] " +
                                        connection.remoteAddress());
                                done.complete(connection);
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG,
                                        "[A] Failure Connect Address to [B] " + multiaddr);
                                done.completeExceptionally(throwable);
                            }
                        });
                    }
                }
                default -> throw new Exception("Protocol " + protocol +
                        " not supported data " + Arrays.toString(data));
            }

        }
    }

    record HopRequest(CompletableFuture<Reservation> done,
                      Multiaddr relayAddress, PeerId self) implements StreamRequester {
        @Override
        public void throwable(Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        @Override
        public void terminated() {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream terminated"));
            }
        }

        @Override
        public void protocol(Stream stream, String protocol) throws Exception {

            if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL,
                    IPFS.RELAY_PROTOCOL_HOP).contains(protocol)) {
                throw new Exception("Token " + protocol + " not supported");
            }
        }

        @Override
        public void fin(Stream stream) {
            // this is invoked, that is good, but request is finished when message is parsed [ok]
        }

        @Override
        public void data(Stream stream, byte[] data) throws Exception {
            Circuit.HopMessage msg = Circuit.HopMessage.parseFrom(data);
            if (msg.getType() == Circuit.HopMessage.Type.STATUS) {
                if (msg.getStatus() == Circuit.Status.OK) {

                    if (msg.hasReservation()) {
                        Circuit.Reservation reserve = msg.getReservation();

                        Limit limit = getLimit(msg);

                        Reservation reservation = new Reservation(limit,
                                stream.connection(), relayAddress,
                                Multiaddr.createCircuit(self, relayAddress),
                                reserve.getExpire());

                        if (LogUtils.isDebug()) {
                            LogUtils.error(TAG, "SUCCESS RESERVATION " +
                                    stream.connection().remotePeerId() + msg);
                        }

                        done.complete(reservation);
                    } else {
                        if (LogUtils.isDebug()) {
                            LogUtils.error(TAG, "NO RESERVATION " +
                                    stream.connection().remotePeerId() + msg);
                        }
                        done.completeExceptionally(new Exception("NO RESERVATION " + msg));
                    }

                } else {
                    if (LogUtils.isDebug()) {
                        LogUtils.error(TAG, "NO RESERVATION " +
                                stream.connection().remotePeerId() + msg);
                    }
                    done.completeExceptionally(new Exception("NO RESERVATION " + msg));
                }
            } else {
                if (LogUtils.isDebug()) {
                    LogUtils.error(TAG, "NO RESERVATION " +
                            stream.connection().remotePeerId() + msg);
                }
                done.completeExceptionally(new Exception("NO RESERVATION " + msg));
            }
        }
    }
}

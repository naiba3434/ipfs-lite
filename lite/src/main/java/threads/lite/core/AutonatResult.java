package threads.lite.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import threads.lite.cid.Multiaddr;

public record AutonatResult(@NonNull NatType natType, @Nullable Multiaddr dialableAddress) {

    // returns NatType (SYMMETRIC, FULL_CONE, RESTRICTED_CONE,
    // PORT_RESTRICTED_CONE, UNKNOWN), only matters when there is no
    // dialable address
    // Note: right now it does not yet distinguish between FULL_CONE, RESTRICTED_CONE,
    // and PORT_RESTRICTED_CONE -> it returns always RESTRICTED_CONE
    @NonNull
    @Override
    public String toString() {
        return "Success " + success() +
                " Address " + dialableAddress() +
                " NatType " + natType();
    }

    public boolean success() {
        return dialableAddress != null;
    }


}

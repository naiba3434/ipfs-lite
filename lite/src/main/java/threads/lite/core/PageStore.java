package threads.lite.core;

import androidx.annotation.NonNull;

import java.util.Date;

import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;

public interface PageStore {
    void storePage(@NonNull Page page);

    Page getPage(@NonNull PeerId peerId);

    void updatePageContent(@NonNull PeerId peerId, @NonNull Cid cid, @NonNull Date eol);

    void clear();

    Cid getPageContent(@NonNull PeerId peerId);
}
